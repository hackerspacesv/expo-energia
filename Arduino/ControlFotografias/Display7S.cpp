#include <Arduino.h>
#include "Display7S.h"

Display7S::Display7S(struct display7s_pin_config &pin_config) {
  // Config stored in array to reduce repetitive coding
  segments[0] = pin_config.A;
  segments[1] = pin_config.B;
  segments[2] = pin_config.C;
  segments[3] = pin_config.D;
  segments[4] = pin_config.E;
  segments[5] = pin_config.F;
  segments[6] = pin_config.G;
  segments[7] = pin_config.DP;
}

void Display7S::begin() {
  for(int i=0;i<8;i++) {
    pinMode(segments[i], OUTPUT);
    digitalWrite(segments[i], HIGH);
  }
}

void Display7S::show(uint8_t value) {
  for(int i=0;i<8;i++) {
    digitalWrite(this->segments[i], (value&(1<<i)) ? LOW : HIGH);
  }
}

void Display7S::print_dec(uint8_t value) {
  uint8_t dot = (value>=10) ? SEG_DP : 0;
  value = value%10;
  switch(value) {
    case 0:
      this->show(CHR_0 | dot);
      break;
    case 1:
      this->show(CHR_1 | dot);
      break;
    case 2:
      this->show(CHR_2 | dot);
      break;
    case 3:
      this->show(CHR_3 | dot);
      break;
    case 4:
      this->show(CHR_4 | dot);
      break;
    case 5:
      this->show(CHR_5 | dot);
      break;
    case 6:
      this->show(CHR_6 | dot);
      break;
    case 7:
      this->show(CHR_7 | dot);
      break;
    case 8:
      this->show(CHR_8 | dot);
      break;
    case 9:
      this->show(CHR_9 | dot);
      break;
  }
}
