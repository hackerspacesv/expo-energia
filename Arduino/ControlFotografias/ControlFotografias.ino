/**
 * ----------------------------------------------------------------------------
 * This is a MFRC522 library example; see https://github.com/miguelbalboa/rfid
 * for further details and other examples.
 *
 * NOTE: The library file MFRC522.h has a lot of useful info. Please read it.
 *
 * Released into the public domain.
 * ----------------------------------------------------------------------------
 * This sample shows how to read and write data blocks on a MIFARE Classic PICC
 * (= card/tag).
 *
 * BEWARE: Data will be written to the PICC, in sector #1 (blocks #4 to #7).
 *
 *
 * Typical pin layout used:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 *
 */
#include <SPI.h>
#include <MFRC522.h>
#include "Display7S.h"
#include "DisplayAnimation.h"
#include "config.h"

// Initialize display
struct display7s_pin_config pin_config = {
    PIN_SEG_A,
    PIN_SEG_B,
    PIN_SEG_C,
    PIN_SEG_D,
    PIN_SEG_E,
    PIN_SEG_F,
    PIN_SEG_G,
    PIN_SEG_DP
};

// Display and animations
Display7S display7s(pin_config);

// Card credit
uint8_t credit = 0;

// Create MFRC522 instance.
MFRC522 mfrc522(SS_PIN, RST_PIN);  
MFRC522::MIFARE_Key key;

// Define card types
enum CARD_CREDIT {
  CARD_POOR = 0,
  CARD_LOW,
  CARD_MED_LOW,
  CARD_MED,
  CARD_MED_HIGH,
  CARD_HIGH,
  CARD_HIGHEST,
  CARD_DEMO   = 0xAA,
  CARD_MASTER = 0xFF
};

// Runtime parameters
enum STA {
  STA_IDLE,
  STA_RECHARGE,
  STA_ANIMATION,
  STA_SHOW_CREDIT
};

enum STA sta = STA_IDLE;

void setup() {
    Serial.begin(9600); // Initialize serial communications with the PC
    while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522 card
    display7s.begin();  // Setup display
    pinMode(RELAY, OUTPUT); // Setup Relay
    digitalWrite(RELAY, LOW);

    // Setup default key 0xFFFFFFFFFF
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    Serial.println(F("Scan a MIFARE Classic PICC to demonstrate read and write."));
    Serial.print(F("Using key (for A and B):"));
    dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println();

    Serial.println(F("BEWARE: Data will be written to the PICC, in sector #1"));
}


/**
 * Main loop.
 */
void loop() {
    byte sector         = NFC_SECTOR;
    byte blockAddr      = NFC_ADDR;
    byte buffer[18];
    byte new_credit = 0;
    byte result = 0;
    byte size = sizeof(buffer);
    byte trailerBlock   = NFC_TRAILER_SHOW;
    MFRC522::StatusCode status;
    MFRC522::PICC_Type piccType;

    // Discount algorithm
    switch(sta) {
      case STA_RECHARGE:
      case STA_IDLE:
        // Update animation
        if(sta== STA_RECHARGE) {
          display7s.show(SEG_E | SEG_G);
        } else {
          display7s.show(SEG_G);
        }
        
        // Check if new card is available
        if( !mfrc522.PICC_IsNewCardPresent() ) goto end_read;
        Serial.println("New card detected...");
        
        // Select one card
        if( !mfrc522.PICC_ReadCardSerial() ) goto end_read;
        Serial.println("Card selected...");
        
        // Read card type to check compatibility
        piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
        if(piccType != MFRC522::PICC_TYPE_MIFARE_MINI
          &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
          &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K ) goto end_read;
        Serial.println("Card is compatible...");

read_data:
        // Authenticate card
        status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
        if (status != MFRC522::STATUS_OK) goto end_read;
        Serial.println("Card authenticated with Key A...");

        // Read data from card
        status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
        if (status != MFRC522::STATUS_OK) goto end_read;
        Serial.println("Data read...");

        // Print data    
        Serial.print(F("Data in block ")); Serial.print(blockAddr); Serial.println(F(":"));
        dump_byte_array(buffer, 16); Serial.println();
        Serial.println();

        // Decide next action based on card type
        switch(buffer[0]) {
          case CARD_POOR:
          case CARD_LOW:
          case CARD_MED_LOW:
          case CARD_MED:
          case CARD_MED_HIGH:
          case CARD_HIGH:
          case CARD_HIGHEST:
            if(sta==STA_IDLE) {
              if(buffer[1]>0) {
                goto discount_credit;
              } else {
                credit = 0;
                sta = STA_SHOW_CREDIT;
                goto stop_crypto;
              }
            }
            if(sta==STA_RECHARGE) {
              goto discount_credit;
            }
            break;
          case CARD_DEMO:
            Serial.println("Showing Demo");
            credit = 0;
            sta = STA_ANIMATION;
            goto stop_crypto;
          case CARD_MASTER:
            sta = STA_RECHARGE;
            goto stop_crypto;
            break;
        }

discount_credit:
        status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, trailerBlock, &key, &(mfrc522.uid));
        if (status != MFRC522::STATUS_OK) goto end_read;
        Serial.println("Card authenticated with Key B...");

        // Discount credit and write
        if(sta==STA_IDLE) {
          new_credit = buffer[1]-1;
        }
        if(sta==STA_RECHARGE) {
          switch(buffer[0]) {
            case CARD_POOR:
              new_credit = CARD_POOR_CREDITS;
              break;
            case CARD_LOW:
              new_credit = CARD_LOW_CREDITS;
              break;
            case CARD_MED_LOW:
              new_credit = CARD_MED_LOW_CREDITS;
              break;
            case CARD_MED:
              new_credit = CARD_MED_CREDITS;
              break;
            case CARD_MED_HIGH:
              new_credit = CARD_MED_HIGH_CREDITS;
              break;
            case CARD_HIGH:
              new_credit = CARD_HIGH_CREDITS;
              break;
            case CARD_HIGHEST:
              new_credit = CARD_HIGHEST_CREDITS;
              break;
            default:
              new_credit = 0;
              break;
          }
        }
        buffer[1] = new_credit;
        status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, buffer, 16);
        if (status != MFRC522::STATUS_OK) goto end_read;
        Serial.println("Data written on card...");

        // Read data from card (again)
        status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
        if (status != MFRC522::STATUS_OK) goto end_read;
        Serial.println("Credit read from card... Validating.");

        if(buffer[1]==new_credit) {
          Serial.println("Credit validated... executing animation.");
          credit = new_credit;
          if(sta==STA_IDLE) {
            sta = STA_ANIMATION;
          }
          if(sta==STA_RECHARGE) {
            sta = STA_SHOW_CREDIT;
          }
          goto stop_crypto;
        }

stop_crypto:
        // Stop card reader
        mfrc522.PICC_HaltA();
        mfrc522.PCD_StopCrypto1();
        Serial.println("Reader stopped...");

end_read:
      break;
    case STA_ANIMATION:
        Serial.println("Starting animation...");
        display7s.show(SEG_DP);
        digitalWrite(RELAY, HIGH);
        delay(10);
        digitalWrite(RELAY, LOW);
        delay(500);
        digitalWrite(RELAY, HIGH);
        delay(50);
        digitalWrite(RELAY, LOW);
        delay(500);
        digitalWrite(RELAY, HIGH);
        delay(10000);
        digitalWrite(RELAY, LOW);
        delay(50);
        digitalWrite(RELAY, HIGH);
        delay(1000);
        digitalWrite(RELAY, LOW);
        delay(50);
        digitalWrite(RELAY, HIGH);
        delay(9000);
        digitalWrite(RELAY, LOW);
        delay(50);
        digitalWrite(RELAY, HIGH);
        delay(1000);
        digitalWrite(RELAY, LOW);

        // Display current credit
        for(int i=0;i<5;i++) {
          display7s.print_dec(credit);
          delay(500);
          display7s.show(CLR);
          delay(500);
        }
        sta = STA_IDLE;
      break;
    case STA_SHOW_CREDIT:
        // Display current credit
        for(int i=0;i<5;i++) {
          display7s.print_dec(credit);
          delay(500);
          display7s.show(CLR);
          delay(500);
        }
        sta = STA_IDLE;
      break;
    };
}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}
