#ifndef CONFIG_H
#define CONFIG_H

// Relay pin
#define RELAY 18

// 7-Segment display pin configuration
#define PIN_SEG_A  15
#define PIN_SEG_B  14
#define PIN_SEG_C   6
#define PIN_SEG_D   7
#define PIN_SEG_E   8
#define PIN_SEG_F  17
#define PIN_SEG_G  16
#define PIN_SEG_DP  5

// MRFC522 pin configuration
#define RST_PIN         9
#define SS_PIN          10

// NFC Memory Configuration
#define NFC_SECTOR          1
#define NFC_ADDR            4
#define NFC_TRAILER_SHOW    7

// NFC Credits
#define CARD_POOR_CREDITS      1
#define CARD_LOW_CREDITS       2
#define CARD_MED_LOW_CREDITS   3
#define CARD_MED_CREDITS       4
#define CARD_MED_HIGH_CREDITS  5
#define CARD_HIGH_CREDITS      7
#define CARD_HIGHEST_CREDITS   10
#endif
