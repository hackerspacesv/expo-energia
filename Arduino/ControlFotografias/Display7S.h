#ifndef DISPLAY_7SEG_H
#define DISPLAY_7SEG_H

/* Element bitmask */
#define SEG_A  0b00000001
#define SEG_B  0b00000010
#define SEG_C  0b00000100
#define SEG_D  0b00001000
#define SEG_E  0b00010000
#define SEG_F  0b00100000
#define SEG_G  0b01000000
#define SEG_DP 0b10000000

/* Common characters */
#define CLR    0x00
#define CHR_0  (SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F)
#define CHR_1  (SEG_B | SEG_C)
#define CHR_2  (SEG_A | SEG_B | SEG_D | SEG_E | SEG_G)
#define CHR_3  (SEG_A | SEG_B | SEG_C | SEG_D | SEG_G)
#define CHR_4  (SEG_B | SEG_C | SEG_F | SEG_G)
#define CHR_5  (SEG_A | SEG_C | SEG_D | SEG_F | SEG_G)
#define CHR_6  (SEG_A | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G)
#define CHR_7  (SEG_A | SEG_B | SEG_C)
#define CHR_8  (SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G)
#define CHR_9  (SEG_A | SEG_B | SEG_C | SEG_D | SEG_F | SEG_G)

struct display7s_pin_config {
  uint8_t A;
  uint8_t B;
  uint8_t C;
  uint8_t D;
  uint8_t E;
  uint8_t F;
  uint8_t G;
  uint8_t DP;
};

class Display7S {
public:
  Display7S(struct display7s_pin_config &pin_config);
  void begin();
  void show(uint8_t value);
  void print_dec(uint8_t value);
private:
  uint8_t segments[8];
};
#endif
