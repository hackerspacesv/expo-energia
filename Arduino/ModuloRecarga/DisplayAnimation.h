#ifndef DISPLAY_ANIMATION_H
#define DISPLAY_ANIMATION_H
#include "Display7S.h"

class DisplayAnimation {
public:
  DisplayAnimation(Display7S &curr_instance);
  void run();
  void clear();
private:
  Display7S &instance;
  uint32_t last_evt;
  uint8_t cur_step;
};
#endif
