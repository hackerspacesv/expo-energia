#include <Arduino.h>
#include "Display7S.h"
#include "DisplayAnimation.h"

DisplayAnimation::DisplayAnimation(Display7S &curr_instance) {
  this->instance = curr_instance;
  this->last_evt = 0;
  this->cur_step = 0;
}

void DisplayAnimation::run() {
  switch(this->cur_step) {
    case 0: 
      this->instance.show(SEG_G);
      if((millis()-this->last_evt)> 25) {
        this->last_evt = millis();
        this->cur_step = 1;
      }
      break;
    case 1: 
      this->instance.show(SEG_B | SEG_C | SEG_E | SEG_F | SEG_G);
      if((millis()-this->last_evt)> 25) {
        this->last_evt = millis();
        this->cur_step = 2;
      }
      break;
    case 2: 
      this->instance.show(SEG_B | SEG_C | SEG_E | SEG_F);
      if((millis()-this->last_evt)> 25) {
        this->last_evt = millis();
        this->cur_step = 3;
      }
      break;
    case 3: 
      this->instance.show(SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F);
      if((millis()-this->last_evt)> 25) {
        this->last_evt = millis();
        this->cur_step = 4;
      }
      break;
    case 4: 
      this->instance.show(SEG_A | SEG_D);
      if((millis()-this->last_evt)> 25) {
        this->last_evt = millis();
        this->cur_step = 5;
      }
      break;
    case 5: 
      this->instance.show(SEG_A | SEG_D | SEG_DP);
      if((millis()-this->last_evt)> 25) {
        this->last_evt = millis();
        this->cur_step = 6;
      }
      break;
    case 6: 
      this->instance.show(SEG_DP);
      if((millis()-this->last_evt)> 25) {
        this->last_evt = millis();
        this->cur_step = 7;
      }
      break;
    case 7: 
      this->instance.show(CLR);
      if((millis()-this->last_evt)> 2000) {
        this->last_evt = millis();
        this->cur_step = 0;
      }
      break;
  }
}

void DisplayAnimation::clear() {
  this->instance.show(CLR);
}
