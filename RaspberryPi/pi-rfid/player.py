#!/usr/bin/env python
import os
import pygame
import time
import random
import RPi.GPIO as GPIO
from SimpleMFRC522HSSV import SimpleMFRC522HSSV

os.environ["SDL_FBDEV"] = "/dev/fb0"

reader = SimpleMFRC522HSSV()

keep_reading = True

# Init screen
screen = None

disp_no = os.getenv("DISPLAY")

if disp_no:
    print("I'm running under X display = {0}".format(disp_no))

drivers = ['fbcon', 'directfb', 'svgalib']

found = False
for driver in drivers:
    if not os.getenv('SDL_VIDEODRIVER'):
        os.putenv('SDL_VIDEODRIVER', driver)
    try:
        pygame.display.init()
    except pygame.error:
        print('Driver: {0} failed.'.format(driver))
        continue
    found = True
    break

if not found:
    raise Exception('No suitable video driver found!')

size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
print("Framebuffer size: %d x %d" % (size[0], size[1]))
screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
screen.fill((0, 0, 0))
pygame.font.init()
pygame.mouse.set_visible(False)
myfont = pygame.font.SysFont("Sans Serif", 150);

try:
  while keep_reading:
    screen.fill((0,0,0))
    pygame.display.update()
    show_video = False
    credits = 0
    id, data = reader.read()
    if data is not None and len(data)==16:
      if ord(data[0]) >= 0 and ord(data[0]) < 7:
        if ord(data[1])>0:
          write_data = []
          write_data.append(ord(data[0]))
          write_data.append(ord(data[1])-1)
          data_txt = ''.join(chr(i) for i in write_data)
          # Try to write data
          reader.write(data_txt)
          id, new_data = reader.read()
          if new_data is not None and len(new_data)==16 and ord(new_data[1]) == write_data[1]:
            show_video = True
            credits = ord(new_data[1])
        else:
          print("Sin creditos")
          textsurface = myfont.render("Sin créditos", False, (255,255,255))
          screen.blit(textsurface,(650,450))
          pygame.display.update()
          time.sleep(5)

      if ord(data[0]) == 0xFF:
        keep_reading = False # End sequence
      if ord(data[0]) == 0xAA:
        show_video = True

    if show_video:
      print("Mostrando Video")
      textsurface = myfont.render("Espera", False, (255, 255, 255))
      screen.blit(textsurface,(650,450))
      pygame.display.update()

      os.popen("omxplayer /home/pi/pi-rfid/HarryTimer1.mov").read()
      screen.fill((0,0,0))
      text = "Tienes {} crédito" if credits == 1 else "Tienes {} créditos"
      textsurface = myfont.render(text.format(credits), False, (255, 255, 255))
      screen.blit(textsurface,(550,450))
      pygame.display.update()
      time.sleep(5)

finally:
    GPIO.cleanup()
