
# Código de Control Expo-Energía

El presente repositorio contiene el código fuente de los módulos de control, visualizador de videos y recarga de tarjetas para la exposición sobre la situación de energía en centroamérica.

La exposición funciona con tarjetas electrónicas tipo NFC que son pre-cargadas con creditos al momento de la entrega. Cada tipo de tarjeta tiene un nivel de credito diferente simulando los recursos disponibles para diferentes grupos poblacionales.

Cada módulo de la exposición descuenta un crédito de la tarjeta y encendiendo las luces que iluminan las fotografias o corriendo el video.

## Materiales
Para construir este proyecto requerirás al menos de los siguientes materiales:

### Módulo de Lámparas
 - 1x Circuito Integrado ATMega328P 
 - 1x Lector NFC RC522 o compatible
 - 1x Relé 5V
 - 1x Transistor 2N2222 o de características similares
 - 1x Resistor de 1K
 - 1x Diodo tipo Schkottky
 - 8x Resistencia de 220Ohm
 - 1x Display LED de 7 Seg
 - 1x Cargador USB de cubo.
 - 1x Caja Plexo para intemperie
 - 6m cable eléctrico 16awg (3 líneas)
 - 1x Toma eléctrico Macho
 - 1x Toma eléctrico Hembra

### Módulo de Video
 - 1x Raspberry Pi 3 B+ o posterior
 - 1x Memoria MicroSD
 - 1x Cargador USB (Al menos 5.2V @2A )

### Módulo de Recarga de Tarjetas
 - 1x Circuito Integrado ATMega328P 
 - 1x Lector NFC RC522 o compatible
 - 1x Cargador USB de cubo.
 - 1x Caja Plexo para intemperie
 - 6m cable eléctrico 16awg (3 líneas)
 - 1x Toma eléctrico Macho

## Requerimientos de Software

 - Una versión reciente del [Arduino IDE](https://www.arduino.cc/en/Main/Software)
 - Librería para el RC522

Abre los archivos de Arduino y Raspberry Pi para acceder al código que controla estos módulos.
 
## Copyright
Este proyecto y el código fuente que le acompaña ha sido elaborado por el
Hackerspace San Salvador para la Expo-Energía. El código fuente se distribuye
bajo la licencia GNU/GPL 3.0 y archivos de diseño o guías de ensamblaje bajo
la licencia Creative Commons 4.0 CC-BY-SA.

